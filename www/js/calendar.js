
function loadCalendarPage() {
	getPortfolioData(loadPortfolioDataCalendarSuccess);
};

function loadPortfolioDataCalendarSuccess(jsonData) {
	var symbolArray = []
	var dateArray = [];
	
	$.each(jsonData, function(index, entry) {
        $.each(entry, function(index, entry) { 
			requestEarning(entry.symbol, function(response) {
				getStockDataCalendarSuccess(response, symbolArray, dateArray);
			});
        });
    });
	
	$(document).ajaxStop(function() {
		var monthNames = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August' , 'September' , 'October', 'November', 'December'];
		
		var calendarInline = myApp.calendar({
			container: '#calendar-inline-container',
			value: [new Date()],
			weekHeader: false,
			toolbarTemplate: 
				'<div class="toolbar calendar-custom-toolbar">' +
					'<div class="toolbar-inner">' +
						'<div class="left">' +
							'<a href="#" class="link icon-only"><i class="icon icon-back"></i></a>' +
						'</div>' +
						'<div class="center"></div>' +
						'<div class="right">' +
							'<a href="#" class="link icon-only"><i class="icon icon-forward"></i></a>' +
						'</div>' +
					'</div>' +
				'</div>',
			onOpen: function (p) {
				$$('.calendar-custom-toolbar .center').text(monthNames[p.currentMonth] +', ' + p.currentYear);
				$$('.calendar-custom-toolbar .left .link').on('click', function () {
					calendarInline.prevMonth();
				});
				$$('.calendar-custom-toolbar .right .link').on('click', function () {
					calendarInline.nextMonth();
				});
			},
			onMonthYearChangeStart: function (p) {
				$$('.calendar-custom-toolbar .center').text(monthNames[p.currentMonth] +', ' + p.currentYear);
			},
			input: '#calendar-events',
			dateFormat: 'M dd yyyy',
			events: dateArray
		});
		
		var table_start = '<table style="width:100%">';
		var arr_length = symbolArray.length;
		for (i = 0; i < arr_length; i++) { 
			table_start += '<tr>' +
											'<td>' + symbolArray[i] +'</td>' +
											'<td>' + 'Earning Release' +'</td>' +
											'<td>' + dateArray[i].toLocaleDateString() + '</td>' +
										'</tr>';
		};		
		
		var table_end = 		'</table>';
		var table_content = $(table_start + table_end);						
		$('#event-detail').append(table_content);
	});
};

function getStockDataCalendarSuccess(data, symbolArray, dateArray) {
	var stockEventsData = processStockEarningData(data);
	symbolArray.push(stockEventsData[0]);
	dateArray.push(new Date(stockEventsData[1]));
};
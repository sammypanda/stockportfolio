function requestEarning(symbol, callback) {
	var url = 'https://www.earningswhispers.com/stocks/' + symbol;
	var finalURL = proxy + url;
	$.ajax({
		url: finalURL,
		async: true,
		crossDomain: true,
		complete: function(data) {
			return callback(data);
		},
		error: function() {
			console.log(symbol + ' earning data fail to get');
		}
	});
};

function requestStock(symbol, callback) {
	var url = 'http://www.nasdaq.com/symbol/' + symbol;
	var finalURL = proxy + url;
	$.ajax({
		url: finalURL,
		async: true,
		crossDomain: true,
		complete: function(data) {
			return callback(data);
		},
		error: function() {
			console.log(symbol + ' stock data fail to get');
		}
	});
};

function processStockEarningData(data) {
	if (data.responseText){
		var responseText = data.responseText;
		
		var symbolRegex = /<div[^<>]*id="mainticker"[^<>]*>[\s\S]*?<\/div>/i;
		var earningRegex = /<div[^<>]*class="mainitem"[\s\S]*?[^<>]*>[\s\S]*?<\/div>/ig;
		var symbol = 'N/A';
		var earning = 'N/A';
		
		if(symbolRegex.test(responseText)) {
			symbol = $(responseText.match(symbolRegex)[0]).text();
		}
		
		if(earningRegex.test(responseText)) {
			earning = $(responseText.match(earningRegex)[1]).text();
		}
		
		var dataArray = [];
		dataArray.push(symbol);
		dataArray.push(earning + ' 2018');
		
		return dataArray;
	}
	return [];
};

function processStockPriceData(data) {
	if (data.responseText){
		var responseText = data.responseText;
		
		var compNameRegex = /<div[^<>]*id="qwidget_pageheader"[^<>]*>[\s\S]*?<\/div>/i;
		var lastSaleRegex = /<div[^<>]*id="qwidget_lastsale"[^<>]*>[\s\S]*?<\/div>/i;
		var netchangeRegex = /<div[^<>]*id="qwidget_netchange"[^<>]*>[\s\S]*?<\/div>/i;
		var netchangePercentRegex = /<div[^<>]*id="qwidget_percent"[^<>]*>[\s\S]*?<\/div>/i;
		var netchangeSignRegex = /qwidget-Red/i;
		
		var compName = $(compNameRegex.exec(responseText)[0]).text();
		var lastSale = $(lastSaleRegex.exec(responseText)[0]).text();
		var sign = netchangeSignRegex.test(responseText) ? '-':'';
		var netchange = sign + $(netchangeRegex.exec(responseText)[0]).text();
		var netchangePercent = sign + $(netchangePercentRegex.exec(responseText)[0]).text();
	
		var dataArray = [];
		dataArray.push(compName.substr(0, compName.indexOf(' Common')));
		dataArray.push(lastSale);
		dataArray.push(netchange);
		dataArray.push(netchangePercent);

		return dataArray;
	}
	
	return [];
};

function processStockNewsData(data) {
	if (data.responseText){
		var responseText = data.responseText;
		
		var newsRegex = /<div[^<>]*id="CompanyNewsCommentary"[^<>]*>[\s\S]*?<\/ul>/i;

		var news = $(newsRegex.exec(responseText)[0]);

		var headlineArray = news.find('a').toArray();
		headlineArray.forEach(function(entry, index, theArray) {
			theArray[index] = entry.text.trim();
		});
		var newsLinkArray = news.find('a').toArray();
		newsLinkArray.forEach(function(entry, index, theArray) {
			theArray[index] = entry.href;
		});
		var newsSourceArray = news.find('small').toArray();
		newsSourceArray.forEach(function(entry, index, theArray) {
			theArray[index] = entry.innerText;
		});
		
		var dataArray = [];
		dataArray.push(headlineArray);
		dataArray.push(newsLinkArray);
		dataArray.push(newsSourceArray);

		return dataArray;
	}
	
	return [];
};

function processKeyStockData(data) {
	if (data.responseText){
		var responseText = data.responseText;
		
		var keyDataRegex = /<div[^<>]*class="row overview-results relativeP"[^<>]*>[\s\S]*?<div class="row">/i;
		var keyData = $(keyDataRegex.exec(responseText)[0]);
		var keyDataArray = keyData.find('.table-cell').toArray();
		var keyDataTextArray = [];
		var keyDataValueArray = [];
		keyDataArray.forEach(function(entry, index, theArray) {
			var trimTxt = $(entry).text().trim();
			if(isOdd(index)) {
				keyDataValueArray.push(trimTxt);
			} else {
				keyDataTextArray.push(trimTxt);
			}
		});
		
		var dataArray = [];
		dataArray.push(keyDataTextArray);
		dataArray.push(keyDataValueArray);
		
		return dataArray;
	}
	
	return [];
};

/*function getStockPriceDataSuccess(data) {
	var result = processStockPriceData(data);
	console.log(result);
}

function getKeyStockDataSuccess(data) {
	var result = processKeyStockData(data);
	console.log(result);
}

function getStockNewsDataSuccess(data) {
	var result = processStockNewsData(data);
	console.log(result);
}*/

//requestStocks(['AHGP','WMT','F','MSFT','FB','BAC','CSCO','LULU','C','AMZN','TSLA','NFLX'], getStockPriceDataSuccess);
//requestStocks(['AHGP','WMT','F','MSFT','FB','BAC','CSCO','LULU','C','AMZN','TSLA','NFLX'], getStockNewsDataSuccess);
//requestStock('NFLX', getKeyStockDataSuccess);








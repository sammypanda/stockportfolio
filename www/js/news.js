
function loadNewsPage() {
	getPortfolioData(loadPortfolioDataNewsSuccess);
};

function loadPortfolioDataNewsSuccess(jsonData) {
	$.each(jsonData, function(index, entry) {
        $.each(entry, function(index, entry) { 
			var dataObj = { symbol : entry.symbol,
							headline : 'headline',
							alink : 'alink',
							source : 'source'};
							
			requestStock(dataObj.symbol, function(response) {
				getStockDataNewsSuccess(response, dataObj);
				var list_item = $('<li/>').addClass('accordion-item');
				var list_item_header  = $('<a href="#" class="item-content item-link">' + 
										'<div class="item-inner">' +
											'<div class="item-title">' +
												'<div class="item-header">' + dataObj.symbol + '</div>' +
											'</div>' +
											'<div class="item-after"><span class="badge">' + dataObj.headline.length + '</span></div>' +
										'</div>' +
									'</a>');
				list_item.append(list_item_header);
				
				var list_item_content_start = '<div class="accordion-item-content">' +
												'<div class="content-block">';
													
				var arr_length = dataObj.headline.length;
				for (i = 0; i < arr_length; i++) { 
					list_item_content_start += '<div><a class="link external" href="' + dataObj.alink[i] + '">' + dataObj.headline[i] + '</a></div>' +
												dataObj.source[i] +
												'<hr>';
				};		
				
				var list_item_content_end = 	'</div>' +
											'</div>';
											
				var list_item_content = $(list_item_content_start + list_item_content_end);						
				list_item.append(list_item_content);
				$('#news-list').append(list_item);
			});
        });
    });
};

function getStockDataNewsSuccess(data, obj) {
	var stockNewsData = processStockNewsData(data);
	obj.headline = stockNewsData[0];
	obj.alink = stockNewsData[1];
	obj.source = stockNewsData[2];
};

function loadHoldingsPage() {
	getPortfolioData(loadPortfolioDataHoldingSuccess);
};

function loadPortfolioDataHoldingSuccess(jsonData) {
	var portfolioTotalMarketValue = 0;
	var portfolioDayReturnValue = 0;
	var portfolioTotalReturnValue = 0;
					
    $.each(jsonData, function(index, entry) {
        $.each(entry, function(index, entry) { 
			var dataObj = { symbol : entry.symbol,
							volume : entry.volume,
							price : entry.price,
							date : entry.date,
							comp_name : 'comp_name',
							change : 'change',
							change_p : 'change_p',
							market_value : 'market_value',
							curr_prc: 'curr_prc',
							key_texts: [],
							key_values: []};
			
			requestStock(dataObj.symbol, function(response) {
				getStockDataHoldingSuccess(response, dataObj);
				var totalGain = (dataObj.market_value - (dataObj.volume * dataObj.price));
				var todayGain = (dataObj.change * dataObj.volume);
				var list_item = $('<li/>').addClass('accordion-item');
				var list_item_header  = $('<a href="#" class="item-content item-link">' + 
										'<div class="item-inner">' +
											'<div class="item-title">' +
												'<div class="item-header">' + dataObj.symbol + ' ' + Number(dataObj.curr_prc).formatMoney(2) + ' | ' + dataObj.change + ' | ' + dataObj.change_p + '</div>' +
												'| Market Value: ' + dataObj.market_value.formatMoney(2) + '<br />' +
												'| Today Gain: ' + todayGain.formatMoney(2) + '<br/>' +
												'| Total Gain: ' + totalGain.formatMoney(2) +
											'</div>' +
											//'<div class="item-after">' + dataObj.VALUE + '</div>' +
										'</div>' +
									'</a>');
				list_item.append(list_item_header);
				
				var list_item_content_start = '<div class="accordion-item-content">' +
												'<div class="content-block">' +
													'<table style="width:100%">';
													
				list_item_content_start += '<tr>' +
													'<td>' + 'Number of Share' +'</td>' +
													'<td class="text-right">' + dataObj.volume + '</td>' +
												'</tr>'
				list_item_content_start += '<tr>' +
													'<td>' + 'Purchase Price' +'</td>' +
													'<td class="text-right">' +  Number(dataObj.price).formatMoney(2) + '</td>' +
												'</tr>'
				list_item_content_start += '<tr>' +
													'<td>' + 'Purchase Date' +'</td>' +
													'<td class="text-right">' + dataObj.date + '</td>' +
												'</tr>'
												
				var arr_length = dataObj.key_texts.length;
				for (i = 0; i < arr_length; i++) { 
					list_item_content_start += '<tr>' +
													'<td>' + dataObj.key_texts[i] +'</td>' +
													'<td class="text-right">' + dataObj.key_values[i] + '</td>' +
												'</tr>';
				};		
				
				var list_item_content_end = 		'</table>' +
														'<hr>' +
													'</div>' +
												'</div>';
				var list_item_content = $(list_item_content_start + list_item_content_end);						
				list_item.append(list_item_content);
				$('#holdings-list').append(list_item);
				
				portfolioTotalMarketValue += Number(dataObj.market_value);
				portfolioDayReturnValue += Number(todayGain);
				portfolioTotalReturnValue += Number(totalGain);
			});
        });
		
		
    });
	
	$(document).ajaxStop(function() {
		$('#holdings-total-value').text(portfolioTotalMarketValue.formatMoney(2));
		$('#holdings-day-return').text(portfolioDayReturnValue.formatMoney(2));
		$('#holdings-total-return').text(portfolioTotalReturnValue.formatMoney(2));
	});
};

function getStockDataHoldingSuccess(data, obj) {
	var stockPriceData = processStockPriceData(data);
	obj.comp_name = stockPriceData[0];
	obj.curr_prc = stockPriceData[1].substr(1);
	obj.change = stockPriceData[2];
	obj.change_p = stockPriceData[3];
	obj.market_value = obj.curr_prc * obj.volume;
	
	var keyStockData = processKeyStockData(data);
	obj.key_texts = keyStockData[0];
	obj.key_values = keyStockData[1];
};
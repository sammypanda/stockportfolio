google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(loadIndexPage);

var chartData = [];
var gainSymbol = [];
var gainValue = [];

function loadIndexPage() {
	$("#piechart").width($(document).width() - 30);
	chartData = [['Symbol', 'Market Value']];
	gainSymbol = [];
	gainValue = [];
	getPortfolioData(loadPortfolioDataIndexSuccess);
};

function loadPortfolioDataIndexSuccess(jsonData) {
	var portfolioTotalMarketValue = 0;
	var portfolioDayReturnValue = 0;
	var portfolioTotalReturnValue = 0;
	
    $.each(jsonData, function(index, entry) {
        $.each(entry, function(index, entry) { 
			var dataObj = { symbol : entry.symbol,
							volume : entry.volume,
							price : entry.price,
							date : entry.date,
							comp_name : 'comp_name',
							change : 'change',
							change_p : 'change_p',
							market_value : 'market_value',
							curr_prc: 'curr_prc',
							key_texts: [],
							key_values: []};
			
			requestStock(dataObj.symbol, function(response) {
				getStockDataIndexSuccess(response, dataObj);
				var totalGain = (dataObj.market_value - (dataObj.volume * dataObj.price));
				var todayGain = (dataObj.change * dataObj.volume);
				
				portfolioTotalMarketValue += Number(dataObj.market_value);
				portfolioDayReturnValue += Number(todayGain);
				portfolioTotalReturnValue += Number(totalGain);
				
				gainSymbol.push(dataObj.symbol);
				gainValue.push(totalGain);
				chartData.push([ dataObj.symbol, parseInt(dataObj.market_value)]);
			});
        });
    });
	
	$(document).ajaxStop(function() {
		$('#index-total-value').text(portfolioTotalMarketValue.formatMoney(2));
		$('#index-day-return').text(portfolioDayReturnValue.formatMoney(2));
		$('#index-total-return').text(portfolioTotalReturnValue.formatMoney(2));
		
		drawChart();
		
		//sort top gainer
		var tempVar;
		for (var i = 0; i < gainValue.length; i++) {
			for (var j = i; j < gainValue.length; j++) {
				if (gainValue[i] < gainValue[j]) {
					tempVar = gainValue[i];
					gainValue[i] = gainValue[j];
					gainValue[j] = tempVar;
					
					tempVar = gainSymbol[i];
					gainSymbol[i] = gainSymbol[j];
					gainSymbol[j] = tempVar;
				}
			}
		}
		
		var top_gainer_length = gainSymbol.length < 5 ? gainSymbol.length : 5;
		for (var i = 0; i < top_gainer_length; i++) {
			var top_gainer_content = $('<div class="row">' + 
										'<div class="col-33">' + gainSymbol[i] + '</div>' + 
										'<div class="col-66">' + Number(gainValue[i]).formatMoney(2) + '</div>'+
									'</div>');
			$('#top-gainer').append(top_gainer_content);
		}
		
		//sort top loser
		var tempVar;
		for (var i = 0; i < gainValue.length; i++) {
			for (var j = i; j < gainValue.length; j++) {
				if (gainValue[i] > gainValue[j]) {
					tempVar = gainValue[i];
					gainValue[i] = gainValue[j];
					gainValue[j] = tempVar;
					
					tempVar = gainSymbol[i];
					gainSymbol[i] = gainSymbol[j];
					gainSymbol[j] = tempVar;
				}
			}
		}
		var top_looser_length = gainSymbol.length < 5 ? gainSymbol.length : 5;
		for (var i = 0; i < top_gainer_length; i++) {
			var top_gainer_content = $('<div class="row">' + 
										'<div class="col-33">' + gainSymbol[i] + '</div>' + 
										'<div class="col-66">' + Number(gainValue[i]).formatMoney(2) + '</div>'+
									'</div>');
			$('#top-loser').append(top_gainer_content);
		}
	});
};

function getStockDataIndexSuccess(data, obj) {
	var stockPriceData = processStockPriceData(data);
	obj.comp_name = stockPriceData[0];
	obj.curr_prc = stockPriceData[1].substr(1);
	obj.change = stockPriceData[2];
	obj.change_p = stockPriceData[3];
	obj.market_value = obj.curr_prc * obj.volume;
	
	var keyStockData = processKeyStockData(data);
	obj.key_texts = keyStockData[0];
	obj.key_values = keyStockData[1];
};

function drawChart() {
	var data = google.visualization.arrayToDataTable(chartData);

	var options = {
	  title: 'Portfolio Allocation'
	};
	
	if ($('#piechart').length) {
		var chart = new google.visualization.PieChart(document.getElementById('piechart'));
		chart.draw(data, options);	
	}
}

